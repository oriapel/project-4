#include "FileStream.h"

#pragma warning (disable:4996)

FileStream::FileStream()
{
	_file = fopen("file.txt", "w");
}

FileStream::~FileStream()
{
	if (_file)
	{
		fclose(_file);
	}
}

FileStream& FileStream::operator<<(const char *str)
{
	if (_file)
	{
		fprintf(_file, "%s", str);
	}
	else
	{
		printf("can't do that\n");
	}
	return *this;
}


FileStream& FileStream::operator<<(int num)
{
	if (_file)
	{
		fprintf(_file, "%d", num);
	}
	else
	{
		printf("can't do that\n");
	}
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	endline(_file);
	return *this;
}

void endline(FILE * file)
{
	if (file)
	{
		fprintf(file, "\n");
	}
	else
		printf("can't do that");
}

