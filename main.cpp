#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <stdio.h>
int main(int argc, char **argv)
{
	OutStream o;
	o <<("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	FileStream f;
	f <<("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	OutStreamEncrypted ceacerCode(3);
	ceacerCode <<("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	Logger log1;
	Logger Log2;
	log1 << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	Log2 << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	getchar();
	return 0;
}
