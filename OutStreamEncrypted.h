#pragma once
#include "OutStream.h"
#include <stdlib.h>


class OutStreamEncrypted:public OutStream
{
private:
	int _cypher;
public:
	OutStreamEncrypted(int cypher);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(char* str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
};