#include "OutStreamEncrypted.h"
#include <string.h>

#pragma warning (disable:4996)

OutStreamEncrypted::OutStreamEncrypted(int cypher)
{
	_cypher = cypher;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(char* str)
{
	char c;
	for (int i = 0; i < strlen(str); i++)
	{	
		c = str[i];
		if (c >= 32 && c <= 126)
		{
			c = char((int(c + _cypher - 32) % 95) + 32);
		}
		printf("%c", c);
	}
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char buffer[100] = { 0 };
	char c;
	sprintf(buffer, "%d", num);
	for (int i = 0; i < strlen(buffer); i++)
	{
		c = buffer[i];
		c = char(int(c + _cypher - 32) % 95 + 32);
		printf("%c", c);
	}
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)())
{
	pf();
	return *this;
}